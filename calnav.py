#!/usr/bin/python
#
# IMPORTANT, NEED TO TEST FOR OVERLAP FROM ONE YEAR TO THE NEXT
import datetime
import calendar
import MySQLdb

from insight import *
"""
day = int(datetime.date.today().strftime("%d"))
month = int(datetime.date.today().strftime("%m"))
year = int(datetime.date.today().strftime("%Y"))

stripped_month = datetime.date.today().strftime("%-m")
"""
"""
day = int(datetime.date(2015,2,27).strftime("%d"))
month = int(datetime.date(2015,2,27).strftime("%m"))
year = int(datetime.date(2015,2,27).strftime("%Y"))
"""
#deez = datetime.datetime(2015,3,23,1,1,1)
deez = datetime.datetime.today()
day = int(deez.strftime("%d"))
month = int(deez.strftime("%m"))
year = int(deez.strftime("%Y"))
#print deez

stripped_month = datetime.date(2015,2,27).strftime("%-m")

days_in_month = calendar.monthrange(year, month)[1]

future = day + 4

if future <= days_in_month:
	#print "sweet, we are working within this month for now"
	# going to need some list action, and a dict
	next_links = []
	next_days = {}

	# lets setup the loop
	for i in range(0,5):
		if i == 0:
			# first day is a link to today.html
			frmt_date = datetime.date(year,month,day).strftime("%b %-d")
			link_date = datetime.date(year,month,day).strftime("%Y%m%d")
			next_days[frmt_date] = link_date
			#next_links.append('http://biz.yahoo.com/research/earncal/today.html')
			url = 'http://biz.yahoo.com/research/earncal/'+str(link_date)+'.html'
			next_links.append(url)
			
					
		else:
			d = day + i
			frmt_date = datetime.date(year,month,d).strftime("%b %-d")
			link_date = datetime.date(year,month,d).strftime("%Y%m%d")
			next_days[frmt_date] = link_date
			url = 'http://biz.yahoo.com/research/earncal/'+str(link_date)+'.html'
			next_links.append(url)

if future > days_in_month:
	next_links = []
	next_days = {}
	days_left = future - days_in_month

	# lets build the list for the current month
	current_month = []
	for i in range(day, days_in_month + 1):
		current_month.append(i)	

	# now to do the same for the next month
	next_month = []
	for ii in range(1,days_left + 1):
		next_month.append(ii)

	# lets trim current_month to account for today.html
	del(current_month[0])

	# now to smash the two lists together and add some special sauce
	next_links = []
	next_days = {}

	link_date = deez.strftime("%Y%m%d")
	url = 'http://biz.yahoo.com/research/earncal/'+str(link_date)+'.html'
	next_links.append(url)

	#next_links.append('http://biz.yahoo.com/research/earncal/today.html')
	for item in current_month:
		url = 'http://biz.yahoo.com/research/earncal/'+str(year)+str(month)+str(item)+'.html'
		next_links.append(url)
		frmt_date = datetime.date(year, month, item).strftime("%b %-d")
		
	for item2 in next_month:
		test_month = month + 1
		if test_month < 10 and item2 < 10:
			# we need to add a leading zero i think
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+'0'+str(test_month)+'0'+str(item2)+'.html'
			next_links.append(url)
			
			frmt_date = datetime.date(year, test_month, item2).strftime("%b %-d")
			
		if test_month > 10 and item2 < 10:
			# only the day needs a leading zero
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+str(test_month)+'0'+str(item2)+'.html'
			next_links.append(url)
			frmt_date = datetime.date(year, test_month, item2).strftime("%b %-d")

		if test_month < 10 and item2 > 10:
			# month needs leading zeros, day does not
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+'0'+str(test_month)+str(item2)+'.html'
			next_links.append(url)

			frmt_date = datetime.date(year, test_month, item2).strftime("%b %-d")
			
#print next_links
for n in next_links:
	db = MySQLdb.connect('localhost','insight','sl0th!','insight')

	cursor = db.cursor()

	dataz = Earnings()

	url1 = n
	tbl_name = dataz.generate_table_name(url1)

	# lets either wipe a table or create a new one
	SQL = 'SELECT 1 FROM '+str(tbl_name)+' LIMIT 1;'
	try:
		cursor.execute(SQL)

	except:
		# time to generate the table to hold this data
		SQL = 'CREATE TABLE '+str(tbl_name)+' (`id` INT(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,`company` VARCHAR(255) NOT NULL,`symbol` VARCHAR(255) NOT NULL,`eps_estimate` VARCHAR(255) NOT NULL,`time` VARCHAR(255) NOT NULL);'
		cursor.execute(SQL)
		db.commit()
	else:
		# table exists, lets drop it and refresh
		SQL = 'DROP TABLE '+str(tbl_name)+';'
		cursor.execute(SQL)
		db.commit()

		# time to generate the table to hold this data
		SQL = 'CREATE TABLE '+str(tbl_name)+' (`id` INT(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,`company` VARCHAR(255) NOT NULL,`symbol` VARCHAR(255) NOT NULL,`eps_estimate` VARCHAR(255) NOT NULL,`time` VARCHAR(255) NOT NULL);'
		cursor.execute(SQL)
		db.commit()
	#d = dataz.get_clean_list('http://biz.yahoo.com/research/earncal/today.html')
	d = dataz.get_clean_list(url1)

	lists = dataz.get_separated_lists(d)
	companies = lists[0]
	symbols = lists[1]
	estimates = lists[2]
	times = lists[3]

	for c in companies:
		i = companies.index(c)
		company = companies[i]
		symbol = symbols[i]
		estimate = estimates[i]
		time = times[i]

		#print company + " " + symbol + " " + estimate + " " + time
		SQL = 'INSERT INTO '+str(tbl_name)+' VALUES (NULL,"'+str(company)+'","'+str(symbol)+'","'+str(estimate)+'","'+str(time)+'");'
		cursor.execute(SQL)
		db.commit()

	db.close()	
