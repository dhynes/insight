#!/usr/bin/python

import datetime
import calendar
import MySQLdb

from insight import *

day = int(datetime.date.today().strftime("%d"))
month = int(datetime.date.today().strftime("%m"))
year = int(datetime.date.today().strftime("%Y"))

strip_month = datetime.date.today().strftime("%-m")
"""
day = datetime.date(2015,2,27).strftime("%d")
month = datetime.date(2015,2,27).strftime("%m")
year = datetime.date(2015,2,27).strftime("%Y")
strip_month = datetime.date(2015,2,27).strftime("%-m")
strip_month = datetime.date.today().strftime("%-m")
"""

num_days = calendar.monthrange(int(year),int(strip_month))[1]

future = int(day) + 4
	# the above line creates a bug when run on any day other than a monday
	# there needs to be some code to make this more intelligent so that daily
	# reports can be run
if future <= num_days:
	#print "future date is still within this month"
	next_days = []
	n_days = {}

	for i in range(1,6):
		if i == 1:
			#next_days.append('http://biz.yahoo.com/research/earncal/today.html')
			frmt_date = datetime.date(year,month,day).strftime("%b %-d")
			link_date = datetime.date.today().strftime("%Y%m%d")
			url = 'http://biz.yahoo.com/research/earncal/'+link_date+'.html'
			n_days[frmt_date] = url
			next_days.append(link_date)
		else:
			d = int(day) + i
			frmt_date = datetime.date(year,month,d).strftime("%b %-d")
			link_date = datetime.date(year,month,d).strftime("%Y%m%d")
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+str(month)+str(d)+'.html'
			n_days[frmt_date] = url 	
			next_days.append(link_date)
			#next_days.append('http://biz.yahoo.com/research/earncal/'+str(year)+str(month)+str(d)+'.html')
if future > num_days:
	next_days = []
	#print "looks like we overlap into the next month"
	days_left = future - num_days
	days_before = 5 - days_left

	month_current = []
	# first lets do the current month
	for i in range(int(day), int(num_days)+1):
		month_current.append(i)
	# we need to remove the first item in this list, its going to be the today.html file
	del(month_current[0])

	# now to build the second list
	month_next = []
	for ii in range(1, int(days_left)+1):
		month_next.append(ii)

	# now its time to put the two lists together in a sensible way
	# first the current month
	link_date = datetime.date.today().strftime("%Y%m%d")
	url = 'http://biz.yahoo.com/research/earncal/'+link_date+'.html'
	next_days.append(url)
	for item in month_current:
		url = 'http://biz.yahoo.com/research/earncal/'+str(year)+'0'+str(month)+str(item)+'.html'
		next_days.append(url)
	for item2 in month_next:
		test_month = int(month) + 1
		if test_month < 10 and item2 < 10:
			# we need to add a leading zero
			next_month = int(month) + 1
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+'0'+str(next_month)+'0'+str(item2)+'.html'
			next_days.append(url)
		if test_month > 10 and item2 < 10:
			# only the day needs leading zeros
			next_month = int(month) + 1
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+str(next_month)+'0'+str(item2)+'.html'
			next_days.append(url)
		if test_month < 10 and item2 > 10:
			# month needs leading zeros, days does not
			next_month = int(month) + 1
			url = 'http://biz.yahoo.com/research/earncal/'+str(year)+'0'+str(next_month)+str(item2)+'.html'
			next_days.append(url)
#for n in next_days:

for n in next_days:
	dataz = Earnings()
	url = n
	tbl_name = dataz.generate_table_name(url)
	print n
	d = dataz.get_clean_list(url)
	print d
	"""
	db = MySQLdb.connect('localhost','insight','sl0th!','insight')

	cursor = db.cursor()

	dataz = Earnings()

	url1 = n
	tbl_name = dataz.generate_table_name(url1)

	# lets either wipe a table or create a new one
	SQL = 'SELECT 1 FROM '+str(tbl_name)+' LIMIT 1;'
	try:
		cursor.execute(SQL)

	except:
		# time to generate the table to hold this data
		SQL = 'CREATE TABLE '+str(tbl_name)+' (`id` INT(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,`company` VARCHAR(255) NOT NULL,`symbol` VARCHAR(255) NOT NULL,`eps_estimate` VARCHAR(255) NOT NULL,`time` VARCHAR(255) NOT NULL);'
		cursor.execute(SQL)
		db.commit()
	else:
		# table exists, lets drop it and refresh
		SQL = 'DROP TABLE '+str(tbl_name)+';'
		cursor.execute(SQL)
		db.commit()

		# time to generate the table to hold this data
		SQL = 'CREATE TABLE '+str(tbl_name)+' (`id` INT(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,`company` VARCHAR(255) NOT NULL,`symbol` VARCHAR(255) NOT NULL,`eps_estimate` VARCHAR(255) NOT NULL,`time` VARCHAR(255) NOT NULL);'
		cursor.execute(SQL)
		db.commit()
	#d = dataz.get_clean_list('http://biz.yahoo.com/research/earncal/today.html')
	d = dataz.get_clean_list(url1)

	lists = dataz.get_separated_lists(d)
	companies = lists[0]
	symbols = lists[1]
	estimates = lists[2]
	times = lists[3]

	for c in companies:
		i = companies.index(c)
		company = companies[i]
		symbol = symbols[i]
		estimate = estimates[i]
		time = times[i]

		#print company + " " + symbol + " " + estimate + " " + time
		SQL = 'INSERT INTO '+str(tbl_name)+' VALUES (NULL,"'+str(company)+'","'+str(symbol)+'","'+str(estimate)+'","'+str(time)+'");'
		cursor.execute(SQL)
		db.commit()

	db.close()	
	"""
